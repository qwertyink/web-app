import { store } from '../store'

export default (to, from, next) => {
  let user = store.getters.fetchUser || {}
  if (Object.keys(user).length > 0) {
    if (user.name) {
      next()
    } else {
      next('/user/edit-info')
    }
  } else {
    next('/login/new-user')
  }
}
