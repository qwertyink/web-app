import Vue from 'vue'
import Router from 'vue-router'
import { store } from '../store'

import ProviderAuthGuard from './provider-auth-guard'
import UserAuthGuard from './user-auth-guard'

const LandingPage = () => import(/* webpackChunkName: "route-landing" */ '@/components/landing/Main')

const UserContainer = () => import(/* webpackChunkName: "route-user-container" */ '@/components/user/MainContainer')
const EditInfo = () => import(/* webpackChunkName: "route-user-edit-info" */ '@/components/user/dashboard/MyInfo')
const FeaturedProducts = () => import(/* webpackChunkName: "route-featured-products" */ '@/components/user/products/FeaturedProducts')
const UserOrders = () => import(/* webpackChunkName: "route-user-orders" */ '@/components/user/order/Main')

const Cart = () => import(/* webpackChunkName: "route-cart" */ '@/components/user/cart/Cart')
const LogIn = () => import(/* webpackChunkName: "route-log-in" */ '@/components/shared/LogInContainer')

const ProviderContainer = () => import(/* webpackChunkName: "route-provider-container" */ '@/components/provider/MainContainer')
const ProviderProfile = () => import(/* webpackChunkName: "route-provider-profile" */ '@/components/provider/profile/Main')
const ProviderProducts = () => import(/* webpackChunkName: "route-provider-products" */ '@/components/provider/products/Main')
const ProviderAddProducts = () => import(/* webpackChunkName: "route-provider-add-products" */ '@/components/provider/products/Add')

const AdminCP = () => import(/* webpackChunkName: "route-provider-container" */ '@/components/admin/Main')

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'LandingPage',
      component: LandingPage
    },
    { path: '/user',
      name: 'UserPage',
      component: UserContainer,
      children: [
        { path: 'products', component: FeaturedProducts },
        { path: 'edit-info', component: EditInfo },
        { path: 'orders', component: UserOrders, beforeEnter: UserAuthGuard },
        {
          path: 'cart',
          component: Cart,
          beforeEnter: (to, from, next) => {
            store.commit('setCartLoading', false)
            next()
          }
        }
      ]
    },
    {
      path: '/provider',
      name: 'ProviderPage',
      component: ProviderContainer,
      children: [
        { path: 'profile', component: ProviderProfile, beforeEnter: ProviderAuthGuard },
        { path: 'products', component: ProviderProducts, beforeEnter: ProviderAuthGuard },
        { path: 'add-products', component: ProviderAddProducts, beforeEnter: ProviderAuthGuard }
      ]
    },
    {
      path: '/admin',
      name: 'AdminPage',
      component: AdminCP
    },
    {
      path: '/login/:type',
      name: 'LogIn',
      component: LogIn,
      props: true
    }
  ],
  mode: 'history'
})
