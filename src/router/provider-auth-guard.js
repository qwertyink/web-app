import { store } from '../store'

export default (to, from, next) => {
  if (store.getters.fetchUser && store.getters.fetchUser.role === 'provider') {
    next()
  } else {
    next('/')
  }
}
