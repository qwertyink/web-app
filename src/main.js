import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import * as firebase from 'firebase'
import { store } from './store'
import Loader from './components/shared/Loading.vue'
import config from './config'
import DateFilter from './filters/date'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'
import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAJ91QtjKRK33KtgDUIQGDvlS6AsS4CDx4',
    v: '3',
    libraries: 'places'
  }
})

Vue.use(VueChartkick, {adapter: Chart})
Vue.use(Vuetify, { theme: {
  primary: '#1A237E',
  secondary: '#2979FF',
  accent: '#FF6F00',
  error: '#f44336',
  warning: '#F57C00',
  info: '#2196f3',
  success: '#4caf50'
}})

Vue.config.productionTip = false

const ErrorAlert = () => import(/* webpackChunkName: "component-error-alert" */ './components/shared/ErrorAlert.vue')
const BottomSheet = () => import(/* webpackChunkName: "component-bottom-sheet" */ './components/user/products/descriptions/BottomSheet')
const DescriptionDialog = () => import(/* webpackChunkName: "component-description-dialog" */ './components/user/products/descriptions/DescriptionDialog')
const ProviderEditDialog = () => import(/* webpackChunkName: "component-provider-edit-dialog" */ './components/provider/profile/components/EditDialog')
const ProviderEditBottomSheet = () => import(/* webpackChunkName: "component-provider-edit-bottom-sheet" */ './components/provider/profile/components/EditBottomSheet')

Vue.filter('date', DateFilter)

Vue.component('app-loading', Loader)
Vue.component('app-bottom-sheet', BottomSheet)
Vue.component('app-error', ErrorAlert)
Vue.component('app-description-dialog', DescriptionDialog)
Vue.component('app-provider-edit-dialog', ProviderEditDialog)
Vue.component('app-provider-edit-bottom-sheet', ProviderEditBottomSheet)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created () {
    firebase.initializeApp(config.firebase)
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        // User is signed in.
        store.dispatch('autoLogin', user)
      } else {
        // User is signed out
        store.dispatch('autoSignout')
      }
    })
  }
})
