import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'

import * as firebase from 'firebase'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    products: [],
    myProducts: [],
    cart: [],
    user: null,
    loading: true,
    compLoading: false,
    cartLoading: false,
    error: null
  },
  getters: {
    cartQuantity (state) {
      return state.cart.length
    },
    products (state) {
      return state.products
    },
    cartItems (state, getters) {
      let matchedProducts = []
      for (let id of state.cart) {
        matchedProducts.push(getters.products.find(product => {
          return product.id === id
        }))
      }
      return matchedProducts
    },
    cartValue (state, getters) {
      let total = 0
      let products = getters.cartItems
      for (let product of products) {
        total += parseInt(product.price)
      }
      return total
    },
    cartLoading (state) {
      return state.cartLoading
    },
    favorites (state) {
      let favorites = []
      for (let index in state.products) {
        if (state.products[index].favorite) {
          favorites.push(state.products[index])
        }
      }
      return favorites
    },
    findProductById (state) {
      return productId => {
        return state.products.find(product => {
          return product.id === productId
        })
      }
    },
    favoriteIndex (state) {
      return favoriteId => {
        return state.products.findIndex(product => {
          return product.id === favoriteId
        })
      }
    },
    fetchUser (state) {
      return state.user
    },
    myProducts (state) {
      return state.myProducts.reverse()
    },
    error (state) {
      return state.error
    },
    isLoading (state) {
      return state.loading
    },
    isLoadingComponent (state) {
      return state.compLoading
    }
  },
  mutations: {
    addToCart (state, payload) {
      state.cart.push(payload)
    },
    removeFromCart (state, index) {
      state.cart.splice(index, 1)
    },
    clearCart (state) {
      state.cart = []
    },
    editFavorite (state, index) {
      state.products[index].favorite = !state.products[index].favorite
    },
    setUser (state, payload) {
      state.user = payload
    },
    setUserBalance (state, payload) {
      state.user.balance = payload
    },
    editUser (state, payload) {
      if (payload.name) {
        state.user.name = payload.name
      }
      if (payload.location) {
        state.user.location = payload.location
      }
      if (payload.description) {
        state.user.description = payload.description
      }
    },
    setProducts (state, payload) {
      state.products = payload
    },
    addProviderProducts (state, payload) {
      state.myProducts = payload
    },
    removeMyProduct (state, payload) {
      state.myProducts.splice(state.myProducts.findIndex(product => {
        return product.key === payload
      }), 1)
    },
    addProviderProduct (state, payload) {
      state.myProducts.push(payload)
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setCartLoading (state, payload) {
      state.cartLoading = payload
    },
    setCompLoading (state, payload) {
      state.compLoading = payload
    },
    setError (state, payload) {
      state.error = payload
    }
  },
  actions: {
    autoLogin ({commit, dispatch}, payload) {
      // 1. Determine the type of user
      firebase.database().ref('/roles/' + payload.uid).once('value')
        .then(snapshot => {
          let role = snapshot.val()
          switch (role) {
            case 'provider':
              dispatch('loadProvider', payload.uid)
              break
            case 'user':
              dispatch('loadUser', payload.uid)
              break
            default:
              commit('setUser', {
                userId: payload.uid,
                phoneNumber: payload.phoneNumber
              })
          }
        })
    },
    autoSignout ({commit}) {
      commit('setUser', null)
    },
    addFavoriteToCart ({ commit, getters }, itemId) {
      let itemIndex = getters.products.findIndex(product => {
        return product.id === itemId
      })
      commit('addToCart', itemIndex)
    },
    clearCart ({commit}) {
      commit('clearCart')
    },
    removeFavorite ({ commit, getters }, itemId) {
      let itemIndex = getters.products.findIndex(product => {
        return product.id === itemId
      })
      commit('editFavorite', itemIndex)
    },
    newProvider ({ getters }) {
      let uid = getters.fetchUser.userId
      return firebase.database().ref('roles/' + uid).set('provider')
        .then(() => {
          firebase.database().ref('providers/' + uid).set({
            approved: false,
            name: 'Unknown',
            location: 'Unknown',
            description: 'No description provided',
            phoneNumber: getters.fetchUser.phoneNumber,
            rating: 0
          })
        })
    },
    loadProvider ({commit}, payload) {
      commit('setLoading', true)
      let uid = payload
      return firebase.database().ref('/providers/' + uid).once('value')
          .then(snapshot => {
            const provider = {
              userId: uid,
              role: 'provider',
              approved: snapshot.val().approved || false,
              name: snapshot.val().name,
              location: snapshot.val().location,
              description: snapshot.val().description,
              phoneNumber: snapshot.val().phoneNumber,
              imageUrl: snapshot.val().imageUrl || '/static/profile-picture.png',
              rating: snapshot.val().rating
            }
            let myproducts = []
            for (let key in snapshot.val().products) {
              let product = {
                key,
                ...snapshot.val().products[key]
              }
              myproducts.push(product)
            }
            commit('setUser', provider)
            commit('addProviderProducts', myproducts)
            commit('setLoading', false)
          })
    },
    loadUser ({commit}, payload) {
      let uid = payload
      return firebase.database().ref('/users/' + uid).once('value')
        .then(snapshot => {
          const user = {
            userId: uid,
            balance: snapshot.val().balance || 0,
            area: snapshot.val().area,
            building: snapshot.val().building,
            name: snapshot.val().name,
            phoneNumber: snapshot.val().phoneNumber
          }
          commit('setUser', user)
        })
    },
    signOut () {
      return firebase.auth().signOut()
    },
    editProviderDetails ({commit, getters}, payload) {
      let userId = getters.fetchUser.userId
      firebase.database().ref('providers/' + userId).update(payload)
        .then(() => {
          commit('editUser', payload)
        })
    },
    editUserDetails ({commit, getters}, payload) {
      const uid = getters.fetchUser.userId
      const userInfo = {
        phoneNumber: getters.fetchUser.phoneNumber,
        ...payload
      }
      return firebase.database().ref('/roles/' + uid).set('user')
        .then(() => {
          return firebase.database().ref('/users/' + uid).set(userInfo)
        })
        .catch(error => {
          console.log('error while saving user info', error)
        })
    },
    uploadProviderPicture ({commit, getters}, payload) {
      const userId = getters.fetchUser.userId
      const fileName = payload.name
      const ext = fileName.slice(fileName.lastIndexOf('.'))
      commit('setLoading', true)
      return firebase.storage().ref('/profile-pictures/' + userId + ext).put(payload)
        .then(fileData => {
          return firebase.database().ref('/providers/' + userId).update({ imageUrl: fileData.metadata.downloadURLs[0] })
        })
        .then(() => {
          return commit('setLoading', false)
        })
        .catch(error => {
          console.log(error)
          return commit('setLoading', false)
        })
    },
    deleteMyProduct ({commit, getters}, payload) {
      let productKey = payload
      firebase.database().ref('/products/' + productKey).remove()
        .then(() => {
          return firebase.database().ref('/providers/' + getters.fetchUser.userId + '/products/' + productKey).remove()
        })
        .then(() => {
          commit('removeMyProduct', productKey)
        })
        .catch(error => {
          console.log(error)
        })
    },
    addItem ({commit, getters}, payload) {
      const product = {
        ...payload,
        providerUid: getters.fetchUser.userId
      }
      let imageUrl, key
      commit('setLoading', true)
      return firebase.database().ref('products').push(product)
        .then(data => {
          key = data.key
          return key
        })
        .then(key => {
          const filename = payload.image.name
          const ext = filename.slice(filename.lastIndexOf('.'))
          return firebase.storage().ref('products/' + key + ext).put(payload.image)
        })
        .then(fileData => {
          imageUrl = fileData.metadata.downloadURLs[0]
          return firebase.database().ref('products').child(key).update({imageUrl: imageUrl})
        })
        .then(() => {
          return firebase.database().ref('/providers/' + getters.fetchUser.userId).child('products/' + key).set({
            ...payload,
            imageUrl
          })
        })
        .then(() => {
          commit('setLoading', false)
          commit('addProviderProduct', {
            ...payload,
            imageUrl
          })
        })
        .catch(error => {
          commit('setLoading', false)
          console.log(error)
        })
    },
    loadProducts ({commit}) {
      let products = []
      // TODO: Add intergration with favorites
      return firebase.database().ref('products').once('value')
        .then(snapshot => {
          let providerKeys = []
          let providerPrms = []
          snapshot.forEach(childSnapshot => {
            let product = {
              id: childSnapshot.key,
              ...childSnapshot.val(),
              favorite: false
            }
            products.push(product)
            // Add provider fetch function to array and avoid duplicates
            let providerUid = childSnapshot.val().providerUid
            if (!providerKeys.includes(providerUid)) {
              providerKeys.push(providerUid)
              providerPrms.push(firebase.database().ref('providers/' + providerUid).once('value'))
            }
          })
          return Promise.all(providerPrms)
        }).then(snapshots => {
          let providers = []
          snapshots.forEach(snapshot => {
            let provider = {
              uid: snapshot.key,
              name: snapshot.val().name,
              location: snapshot.val().location,
              phoneNumber: snapshot.val().phoneNumber
            }
            providers.push(provider)
          })
          products.forEach(product => {
            let productProvider = providers.find(provider => {
              return provider.uid === product.providerUid
            })
            product.providerName = productProvider.name
            product.providerLocation = productProvider.location
            product.providerPhoneNumber = productProvider.phoneNumber
          })
          commit('setProducts', products)
        }).catch(error => {
          console.log('Error fetching products: ', error)
        })
    },
    saveOrder ({commit, getters}, payload) {
      let items = {}
      let totalAmount = 0
      for (let item of getters.cartItems) {
        let id = item.id
        totalAmount += parseInt(item.price)
        if (items[id]) {
          items[id].amount += 1
        } else {
          items[id] = {
            name: item.name,
            price: item.price,
            amount: 1,
            providerPhoneNumber: item.providerPhoneNumber,
            providerName: item.providerName,
            providerLocation: item.providerLocation
          }
        }
      }
      let order = {
        created: new Date().toISOString(),
        userphoneNumber: getters.fetchUser.phoneNumber,
        totalAmount: totalAmount,
        items,
        ...payload
      }
      return firebase.database().ref('/orders/' + getters.fetchUser.userId).push(order)
        .then(() => {
          return order
        })
        .catch(error => {
          console.log(error)
        })
    },
    loadOrders ({commit, getters}) {
      return firebase.database().ref('/orders/' + getters.fetchUser.userId).once('value')
        .then(snapshot => {
          let orders = []
          snapshot.forEach(childSnapshot => {
            let order = {
              id: childSnapshot.key,
              ...childSnapshot.val()
            }
            orders.push(order)
          })
          return orders
        })
    },
    loadBalance ({commit, getters}) {
      return firebase.database().ref('/users/' + getters.fetchUser.userId + '/balance/').once('value')
        .then(snapshot => {
          commit('setUserBalance', snapshot.val())
        })
        .catch(error => {
          console.log(error)
        })
    },
    payForOrder ({ commit, getters }, payload) {
      let balance = 0
      return firebase.database().ref('/users/' + getters.fetchUser.userId + '/balance/').once('value')
        .then(snapshot => {
          balance = snapshot.val()
          if (balance < payload.totalAmount) {
            throw new Error('Your account balance is insufficient')
          }
          balance -= payload.totalAmount
          commit('setUserBalance', balance)
          return Promise.all([
            firebase.database().ref('/users/' + getters.fetchUser.userId + '/balance/').set(balance),
            firebase.database().ref('/orders/' + getters.fetchUser.userId + '/' + payload.id).update({processed: true})
          ])
        })
        .then(() => {
          let data = {
            items: payload.items,
            location: payload.location,
            userphoneNumber: payload.userphoneNumber
          }
          return Axios.post('https://mydelivary-server.herokuapp.com/sms', data)
        })
    },
    deleteOrder ({ commit, getters }, payload) {
      return firebase.database().ref('/orders/' + getters.fetchUser.userId + '/' + payload).remove()
    },
    loadStatsForAdmin () {
      let stats = {
        orders: [],
        users: [],
        providers: []
      }
      return firebase.database().ref('/orders/').once('value')
        .then(userSnapshot => {
          userSnapshot.forEach(orderSnapshot => {
            orderSnapshot.forEach(childSnapshot => {
              let order = {
                key: childSnapshot.key,
                ...childSnapshot.val()
              }
              stats.orders.push(order)
            })
          })
          return firebase.database().ref('/users/').once('value')
        })
        .then(snapshot => {
          snapshot.forEach(childSnapshot => {
            let user = {
              uid: childSnapshot.key,
              ...childSnapshot.val()
            }
            stats.users.push(user)
          })
          return firebase.database().ref('/providers/').once('value')
        })
        .then(snapshot => {
          snapshot.forEach(childSnapshot => {
            let provider = {
              uid: childSnapshot.key,
              ...childSnapshot.val()
            }
            stats.providers.push(provider)
          })
          return stats
        })
    },
    setError ({commit}, payload) {
      commit('setError', payload)
    },
    clearError ({commit}, payload) {
      commit('setError', null)
    }
  }
})
