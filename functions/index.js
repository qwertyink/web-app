const functions = require('firebase-functions')
const admin = require('firebase-admin')
const auth = require('basic-auth')

admin.initializeApp(functions.config().firebase)

exports.paymentAPI = functions.https.onRequest((request, response) => {
  if (request.method != 'POST') {
    return response.status(401).send('Method not allowed')
  }
  
  const credentials = functions.config().credentials
  const incomingCredentials = auth(request) || { name: null, pass: null }

  if (incomingCredentials.name != credentials.name || incomingCredentials.pass != credentials.pass) {
    return response.status(401).send('Unauthorized access')
  }

  const paymentId = request.body.transaction_reference
  const paymentDetails = {
    amount: request.body.amount,
    phoneNumber: request.body.sender_phone,
    firstName: request.body.first_name,
    middleName: request.body.middle_name,
    lastName: request.body.last_name,
    time: request.body.transaction_timestamp
  }
  let uid = null
  
  admin.database().ref('/payments/' + paymentId).set(paymentDetails)
    .then(() => {
      return admin.auth().getUserByPhoneNumber(paymentDetails.phoneNumber)      
    })
    .then(userObject => {
      uid = userObject.uid
      return admin.database().ref('/users/' + userObject.uid).once('value')
    })
    .then(snapshot => {
      let balance = snapshot.val().balance || 0
      balance += paymentDetails.amount
      return admin.database().ref('/users/' + uid + '/balance/').set(balance)
    })
    .then(() => {
      return response.status(200).json({
        status: '01',
        description: 'Accepted',
        subscriber_message: 'Your payment of ' + paymentDetails.amount + ' was received. Please await confirmation about your order. Regards https://www.myDelivary.com'
      })
    })
    .catch(error => {
      console.log('PaymentAPI ran into an error: ', error)
      return response.status(200).json({
        status: '01',
        description: 'Accepted'
      })
    })
})
